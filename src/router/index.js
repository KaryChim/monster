import Vue from 'vue'
import Router from 'vue-router'
import home from '../components/home/home'
import login from '../components/home/login'
import register from '../components/home/register'
import member from  '../components/member/member'
import result from  '../components/member/result'
import suggest from '../components/member/suggest'
import study from '../components/study/study'
import basisPractice from "../components/study/basisPractice/basisPractice";
import starPractice from '../components/study/levelPractice/starPractice'
import starExam from '../components/study/exam/starExam'



Vue.use(Router)

export default new Router({
  routes: [
    {path: '/', redirect: '/home/login'},
    {path: '/home', name: 'home', component: home},
    {path: '/home/login', name: 'login',component: login},
    {path: '/home/register', name: 'register',component: register},
    {path: '/member', name: 'member',component: member ,meta:{requireAuth: true}},
    {path: '/member/result', name: 'result',component: result ,meta:{requireAuth: true}},
    {path: '/member/suggest', name: 'suggest',component: suggest ,meta:{requireAuth: true}},
    {path: '/study', name: 'study',component: study ,meta:{requireAuth: true}},
    {path: '/study/basisPractice/basisPractice', name: 'basisPractice',component:basisPractice ,meta:{requireAuth: true}},
    {path: '/study/levelPractice/starPractice', name: 'starPractice',component:starPractice ,meta:{requireAuth: true}},
    {path: '/study/exam/starExam', name: 'starExam',component:starExam ,meta:{requireAuth: true}},
  ]



})
