import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router'
import store from './store/store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.bubble.css'
import 'quill/dist/quill.snow.css'
import 'element-ui/lib/theme-chalk/index.css'


Vue.config.productionTip = false


Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(ElementUI)
Vue.use(VueQuillEditor)



router.beforeEach((to, from, next) => {
    const token = store.state.token
    if (to.meta.requireAuth){
        if (token){
            next()
        } else {
            next({
                path: '/home/login',
            })
        }
    } else {
        next()
    }
})


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

