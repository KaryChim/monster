import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    isLogin: window.sessionStorage.getItem('isLogin'),
    user: window.sessionStorage.getItem('user'),
    token: window.sessionStorage.getItem('token'),
    refreshToken: window.sessionStorage.getItem('refreshToken')

}

const mutations = {
    //获取token并将token保存到sessionStrorage
    set_token: (state, data) => {
        state.token = data
        window.sessionStorage.setItem("token",data)
        console.log(state.token)
    },
    //获取refreshToken并将refreshToken保存到sessionStrorage
    set_refreshToken: (state, data) => {
        state.refreshToken = data
        window.sessionStorage.setItem("refreshToken",data)
        console.log(state.refreshToken)
    },

    //将登录状态设置为true
    set_login: state => {
        state.isLogin = true
        window.sessionStorage.setItem("isLogin","true")
    },

    //获取用户名并将用户名保存到sessionStrorage
    set_user: (state, data) => {
        state.user = data
        window.sessionStorage.setItem("user", data)
    },


    //清除token，退出登录
    exit_login: state => {
        state.user = null
        state.token = null
        state.isLogin = false
        state.refreshToken = null
        window.sessionStorage.removeItem('user')
        window.sessionStorage.removeItem('token')
        window.sessionStorage.removeItem('isLogin')
        window.sessionStorage.removeItem('refreshToken')
    }


}

const action = {

}

const getters = {

}

export default new Vuex.Store({
    state,
    mutations,
    action,
    getters

})

