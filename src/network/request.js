import axios from 'axios'

export function loginRequest(config) {
    const instance = axios.create({
        baseURL: 'http://49.235.201.4:8080',
        timeout: 5000
    })
  return instance(config)
}


export function praticeRequest(config) {
    const instance = axios.create({
        baseURL: 'http://49.235.201.4:8080',
        headers: {
            'Content-Type': 'multipart/form-data',
            'authorization': window.sessionStorage.getItem("token")
        }
    })

    return instance(config)
}

export function mumRequest(config) {
    const instance = axios.create({
        baseURL: 'http://49.235.201.4:8080',
        headers: {
            'authorization': window.sessionStorage.getItem("token")
        }
    })

    return instance(config)
}
